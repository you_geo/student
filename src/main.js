// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueParticles from 'vue-particles'
import 'element-ui/lib/theme-chalk/index.css'
import './assets/icon/iconfont.css'
import axios from 'axios'
import {
  ColorPicker,
  Pagination,
  Dialog,
  Input,
  Radio,
  RadioGroup,
  RadioButton,
  Checkbox,
  CheckboxButton,
  CheckboxGroup,
  Select,
  Option,
  OptionGroup,
  Button,
  ButtonGroup,
  Table,
  TableColumn,
  DatePicker,
  TimeSelect,
  TimePicker,
  Form,
  FormItem,
  Tabs,
  TabPane,
  Tag,
  Icon,
  Row,
  Col,
  Upload,
  PageHeader,
  Message,
  Footer,
  Dropdown,
  DropdownMenu,
  DropdownItem
} from 'element-ui'
import VueQuillEditor from 'vue-quill-editor'
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'
// 引入echarts
import echarts from 'echarts'
import htmlToPdf from './htmlToPdf.'
Vue.use(VueQuillEditor)
Vue.use(Dropdown)
Vue.use(DropdownItem)
Vue.use(DropdownMenu)
Vue.use(ColorPicker)
Vue.use(Footer)
Vue.use(Pagination)
Vue.use(Dialog)
Vue.use(Input)
Vue.use(Radio)
Vue.use(RadioGroup)
Vue.use(RadioButton)
Vue.use(Checkbox)
Vue.use(CheckboxButton)
Vue.use(CheckboxGroup)
Vue.use(Select)
Vue.use(Option)
Vue.use(OptionGroup)
Vue.use(Button)
Vue.use(ButtonGroup)
Vue.use(Table)
Vue.use(TableColumn)
Vue.use(DatePicker)
Vue.use(TimeSelect)
Vue.use(TimePicker)
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Tabs)
Vue.use(TabPane)
Vue.use(Tag)
Vue.use(Icon)
Vue.use(Row)
Vue.use(Col)
Vue.use(Upload)
Vue.use(PageHeader)
Vue.use(VueParticles)
Vue.use(htmlToPdf)

Vue.prototype.$echarts = echarts
Vue.prototype.$http = axios
Vue.prototype.$main = ''
Vue.prototype.$url = ''
Vue.prototype.$message = Message
Vue.config.productionTip = false
// axios.interceptors.request.use(
//   config => {
//   // 判断是否存在token，如果存在的话，则每个http header都加上token
//     let token = localStorage.getItem('Authorization')
//     if (!config.headers.hasOwnProperty('Authorization') && token) {
//       config.headers.Authorization = token
//     }
//     return config
//   },
//   error => {
//     return Promise.reject(error)
//   })
/* eslint-disable no-new */
// 在main.js设置全局的请求次数，请求的间隙
// axios.defaults.retry = 4
// axios.defaults.retryDelay = 1000

// axios.interceptors.response.use(undefined, function axiosRetryInterceptor(err) {
//   var config = err.config
//   // If config does not exist or the retry option is not set, reject
//   if (!config || !config.retry) return Promise.reject(err)

//   // Set the variable for keeping track of the retry count
//   config.__retryCount = config.__retryCount || 0

//   // Check if we've maxed out the total number of retries
//   if (config.__retryCount >= config.retry) {
//     // Reject with the error
//     return Promise.reject(err)
//   }

//   // Increase the retry count
//   config.__retryCount += 1

//   // Create new promise to handle exponential backoff
//   var backoff = new Promise(function(resolve) {
//     setTimeout(function() {
//       resolve()
//     }, config.retryDelay || 1)
//   })

//   // Return the promise in which recalls axios to retry the request
//   return backoff.then(function() {
//     return axios(config)
//   })
// })

new Vue({
  el: '#app',
  router,

  components: { App },
  template: '<App/>'

})
