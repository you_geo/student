import Vue from 'vue'
import Router from 'vue-router'
import mainMenu from '../components/mainMenu'
import report from '../components/report'
import login from '../components/login/login'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/main',
      name: 'mainMenu',
      component: mainMenu
    }, {
      path: '/report',
      name: 'report',
      component: report
    }, {
      path: '/',
      name: 'login',
      component: login
    }
  ]

})
router.beforeEach((to, from, next) => {
  if (to.path === '/') {
    next()
  } else {
    let token = localStorage.getItem('Authorization')

    if (!token) {
      next('/')
    } else {
      next()
    }
  }
})

export default router
